package com.myhome.configuration;
import com.myhome.configuration.properties.mail.EmailTemplateLocalizationProperties;
import com.myhome.configuration.properties.mail.EmailTemplateProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import java.util.Locale;

@Configuration
@RequiredArgsConstructor
public class EmailTemplateConfig {
  private final EmailTemplateProperties templateProperties;
  private final EmailTemplateLocalizationProperties localizationProperties;

  
  @Bean
  public ResourceBundleMessageSource emailMessageSource() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasename(localizationProperties.getPath());
    messageSource.setDefaultLocale(Locale.ENGLISH);
    messageSource.setDefaultEncoding(localizationProperties.getEncoding());
    messageSource.setCacheSeconds(localizationProperties.getCacheSeconds());
    return messageSource;
  }

  /**
   * creates a Spring Template Engine instance with Thymeleaf-specific templates and
   * message source for email messages.
   * 
   * @param emailMessageSource message source for email-related messages, providing the
   * necessary translations for the Thymeleaf template engine to generate email-related
   * templates.
   * 
   * 	- The `ResourceBundleMessageSource` class is responsible for providing localization
   * messages to the Thymeleaf engine.
   * 	- The `emailMessageSource` object can be used to specify custom message sources
   * for the Thymeleaf engine.
   * 	- The `templateEngineMessageSource` property is set to `emailMessageSource`,
   * indicating that this object will provide message sources for the Thymeleaf engine.
   * 
   * @returns a Spring Template Engine instance configured to use Thymeleaf as the
   * template engine and an email message source for message resolution.
   * 
   * 	- `SpringTemplateEngine`: This is the template engine instance created using
   * Spring's `SpringTemplateEngine` class.
   * 	- `templateResolver()`: This is a reference to the `ThymeleafTemplateResolver`
   * instance that resolves Thymeleaf templates.
   * 	- `emailMessageSource()`: This is the message source for email-related messages.
   */
  @Bean
  public SpringTemplateEngine thymeleafTemplateEngine(ResourceBundleMessageSource emailMessageSource) {
    SpringTemplateEngine templateEngine = new SpringTemplateEngine();
    templateEngine.setTemplateResolver(thymeleafTemplateResolver());
    templateEngine.setTemplateEngineMessageSource(emailMessageSource);
    return templateEngine;
  }


  /**
   * creates a Thymeleaf Template Resolver, setting its prefix, suffix, template mode,
   * character encoding and cacheability based on properties.
   * 
   * @returns a `ITemplateResolver` instance configured to resolve Thymeleaf templates
   * based on properties.
   * 
   * 	- `setPrefix()`: sets the prefix of the template path. If the template path ends
   * with a file separator, it is not included in the prefix.
   * 	- `setSuffix()`: sets the suffix of the template path. This property determines
   * the format of the template.
   * 	- `setTemplateMode()`: sets the template mode, which can be either "HTML" or "XML".
   * 	- `setCharacterEncoding()`: sets the character encoding of the template.
   * 	- `setCacheable()`: indicates whether the template is cacheable or not.
   */
  private ITemplateResolver thymeleafTemplateResolver() {
    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();

    String templatePath = templateProperties.getPath();
    String fileSeparator = System.getProperty("file.separator");
    templateResolver.setPrefix(templatePath.endsWith(fileSeparator) ? templatePath : templatePath + fileSeparator);

    templateResolver.setSuffix(templateProperties.getFormat());
    templateResolver.setTemplateMode(templateProperties.getMode());
    templateResolver.setCharacterEncoding(templateProperties.getEncoding());
    templateResolver.setCacheable(templateProperties.isCache());
    return templateResolver;
  }

}